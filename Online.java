public class Online extends Course{
    public String onlineRoom;

    public Online(String type, String course, String professor, String department, String onlineRoom) {
        super(type, course, professor, department);
        this.onlineRoom = onlineRoom;
    }

    public String getOnlineRoom() {
        return onlineRoom;
    }

    public void setOnlineRoom(String onlineRoom) {
        this.onlineRoom = onlineRoom;
    }
}
