import java.util.*;
import java.io.IOException;
import java.nio.*;
import java.nio.file.*;

public class CourseReader {
    public static void main(String[] args) throws Exception{
        List<String> courses1 = new ArrayList<String>();
        List<String> courses2 = new ArrayList<String>();

        courses1 = getCourses("courses.csv");

        System.out.println("getCourses method:" + "\n");
        for (int i = 0; i < courses1.size(); i++){
            System.out.println(courses1.get(i));
        }

        System.out.println("\n" + "getCoursesTaught method:" + "\n");
        courses2 = getCoursesTaught("courses.csv", "Dr. Professorton");

        for (int i = 0; i < courses2.size(); i++){
            System.out.println(courses2.get(i));
        }
    }


    public static List<String> getCourses(String filename) throws IOException{
        List<String> courses = new ArrayList<String>();
        List<String> data = new ArrayList<String>();

        Path p = Paths.get(filename);
        data = Files.readAllLines(p);

        for (int i = 0; i < data.size(); i++){
            String[] parts = data.get(i).split(",");
            if (!parts[0].equals("group")){
                courses.add(parts[1]);
            }
        }

        return courses;
    }

    public static List<String> getCoursesTaught(String filename, String teacherName) throws IOException{
        List<String> courses = new ArrayList<String>();
        List<String> data = new ArrayList<String>();

        Path p = Paths.get(filename);
        data = Files.readAllLines(p);

        for (int i = 0; i < data.size(); i++){
            String[] parts = data.get(i).split(",");
            if (parts[2].equals(teacherName)){
                courses.add(parts[1]);
            }
        }

        return courses;
    }


}
