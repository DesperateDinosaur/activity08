public abstract class Course implements Meeting{
    public String type;
    public String course;
    public String professor;
    public String department;

    public Course(String type, String course, String professor, String department){
        this.type = type;
        this.course = course;
        this.professor = professor;
        this.department = department;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
