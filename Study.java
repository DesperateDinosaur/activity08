public class Study implements Meeting{
    public String type;
    public String group;
    public String studyRoom;

    public Study(String type, String group, String studyRoom){
        this.type = type;
        this.group = group;
        this.studyRoom = studyRoom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getStudyRoom() {
        return studyRoom;
    }

    public void setStudyRoom(String studyRoom) {
        this.studyRoom = studyRoom;
    }
}
